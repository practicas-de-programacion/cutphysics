using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public GameEvent mEvent;

    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine("Broadcast");
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Broadcast()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3f, 5f));

            mEvent.RaiseConDestination(new Vector3(this.transform.position.x + Random.Range(-1,1), this.transform.position.y, this.transform.position.z + Random.Range(-1, 1)));

        }
       
    }

}
