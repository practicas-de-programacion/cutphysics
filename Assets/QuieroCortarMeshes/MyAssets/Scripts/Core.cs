using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Core : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.transform.tag.Equals("Blade"))
        {

            if (this.gameObject.GetComponentInParent<BasicEnemy>() != null)
            {
                this.gameObject.GetComponentInParent<BasicEnemy>().onCoreDestroy();
              
            }
            if (this.gameObject.GetComponentInParent<BossShield>() != null)
            {
                this.gameObject.GetComponentInParent<BossShield>().onCoreDestroy();
                
            }
            

            Destroy(this.gameObject);

        }

    }

}
