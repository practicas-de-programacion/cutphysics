using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShield : MonoBehaviour
{

    private int mCores;

    // Start is called before the first frame update
    void Start()
    {
        mCores = 3;
    }

    // Update is called once per frame
    void Update()
    {
     
        this.transform.Rotate(0, 0, 3);

    }

    public void onCoreDestroy()
    {
        mCores--;
        if(mCores <= 0)
        {
           this.GetComponentInParent<Cut>().canCut = true;
        }
    }


}
