using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightParticles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawn()
    {
        while (true)
        {

            yield return new WaitForSeconds(UnityEngine.Random.Range(1, 10));

            this.GetComponent<ParticleSystem>().Play();

        }
    }
}
