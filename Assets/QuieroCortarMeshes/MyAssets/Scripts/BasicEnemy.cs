using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BasicEnemy : MonoBehaviour
{


    private Cut mCut;
    public GameObject mCore;
    public Vector3 mTargetPosition;
    private NavMeshAgent nav;
    // Start is called before the first frame update
    void Start()
    {

        mTargetPosition = this.transform.position;

        mCut = this.GetComponent<Cut>();
        mCut.canCut = false;

        nav = this.GetComponent<NavMeshAgent>();

        nav.updateRotation = false;
        nav.updateUpAxis = false;

        this.transform.Rotate(new Vector3(0, UnityEngine.Random.Range(-180, 180), 0));

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onCoreDestroy()
    {
        new WaitForSeconds(0.5f);
        mCut.canCut = true;
    }

    public void newTarget()
    {
        if(UnityEngine.Random.Range(0,2) > 0)
        {

            var pos = new Vector3(mTargetPosition.x + UnityEngine.Random.Range(-2,2), mTargetPosition.y, mTargetPosition.z + UnityEngine.Random.Range(-2, 2));
            nav.SetDestination(mTargetPosition);
        }
        else
        {
            var pos = new Vector3(this.transform.position.x + UnityEngine.Random.Range(-2, 2), this.transform.position.y, this.transform.position.z + UnityEngine.Random.Range(-2, 2));
            nav.SetDestination(mTargetPosition);
        }
       
        //print(mTargetPosition);
        //print("AAAAAAAAAA");
    }

}
