using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class OneShotParticlesScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<ParticleSystem>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.GetComponent<ParticleSystem>().isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
