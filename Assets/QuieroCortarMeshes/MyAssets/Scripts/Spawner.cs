using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{


    public GameObject Enemy;

    public GameEvent mEvent;

    public GameObject ParticlesWhenDead;

    public int SpawnTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawn()
    {
        while (true) { 
        
            yield return new WaitForSeconds(SpawnTime);

            if(UnityEngine.Random.Range(1, 10) > 5)
            {
                GameObject newEnemy = Instantiate(Enemy);
                Enemy.transform.position = new Vector3(this.transform.position.x + 1, this.transform.position.y, this.transform.position.z);
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > this.GetComponent<Cut>().MinImpactToBreak && collision.transform.tag.Equals("Blade"))
        {

            GameObject particles = Instantiate(ParticlesWhenDead);
            particles.transform.position = this.transform.position;
            mEvent.Raise();


        }
    }
}
