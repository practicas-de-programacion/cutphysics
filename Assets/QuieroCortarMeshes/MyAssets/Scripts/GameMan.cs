using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMan: MonoBehaviour
{

    public GameObject Boss;
    private int mSpawners;

    // Start is called before the first frame update
    void Start()
    {
        mSpawners = 4;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onSpawnerDeath()
    {
        mSpawners--;
        if (mSpawners <= 0)
        {
            GameObject mboss = Instantiate(Boss);
            mboss.transform.position = this.transform.position;
        }
    }

}
