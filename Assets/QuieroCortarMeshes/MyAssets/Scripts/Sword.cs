using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public int SwingSoundForce;

    public Material ChargedMaterial;
    public Material SpecialMaterial;
    private Material mMaterial;
    public GameObject mSlash;

    private bool charged;

    // Start is called before the first frame update
    void Start()
    {
        charged = false;
        mMaterial = this.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
       
        if(charged && this.gameObject.GetComponent<Rigidbody>().velocity.magnitude > SwingSoundForce)
        {
            UnchargeSlash();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;

        if (this.gameObject.GetComponent<Rigidbody>().velocity.magnitude > SwingSoundForce)
        {
            this.gameObject.GetComponent<AudioSource>().Play();
        }
    }

    public void chargeSlash()
    {
        this.gameObject.GetComponent<MeshRenderer>().material = ChargedMaterial;
        charged = true;
    }

    public void UnchargeSlash()
    {
        this.gameObject.GetComponent<MeshRenderer>().material = mMaterial;
        GameObject newSlash = Instantiate(mSlash);
        newSlash.transform.localScale = this.transform.localScale;
        newSlash.GetComponent<MeshRenderer>().material = SpecialMaterial;
        newSlash.transform.position = this.transform.position;
        newSlash.transform.up = this.gameObject.GetComponent<Rigidbody>().velocity.normalized *-1;
        newSlash.GetComponent<Rigidbody>().AddForce(newSlash.transform.up.normalized*-200);
        charged = false;
    }

}
