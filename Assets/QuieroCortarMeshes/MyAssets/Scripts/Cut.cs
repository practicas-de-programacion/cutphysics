using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Cut : MonoBehaviour
{

    public float MinImpactToBreak;
    public float cutCD;
    public float CutJumpForce;
    public float minSize;
    public bool canCut;
    public int deathTimer;


    private bool flag;
    //public GameObject tumor;

    // Start is called before the first frame update
    void Start()
    {
        flag = false;
        StartCoroutine("Intangible");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.relativeVelocity.magnitude > MinImpactToBreak && coll.transform.tag.Equals("Blade") && canCut)
        {

            if (this.transform.localScale.x > minSize || this.transform.localScale.y > minSize || this.transform.localScale.z > minSize)
            {

           
                if (flag)
                {
                    flag = false;

                    Vector3 Breackpoint = coll.GetContact(0).point;
                    Vector3 BreackForward = coll.relativeVelocity.normalized;
                    //GameObject impactpoint = Instantiate(tumor);

                    //impactpoint.transform.position = Breackpoint;           

                    //impactpoint.transform.forward = coll.relativeVelocity.normalized;

                    //Debug.DrawRay(impactpoint.transform.position, Breackpoint - coll.transform.forward.normalized * 0.1f * 100, Color.blue, 100);


                    this.transform.gameObject.layer = 10;

                    int layerMask = LayerMask.GetMask("Ray");

                    Vector3 startRaycastPosition = Breackpoint + BreackForward.normalized * 2;


                    RaycastHit hit;
                    if (Physics.Raycast(startRaycastPosition, BreackForward * -1, out hit, 100, layerMask))
                    {
                        Debug.DrawRay(startRaycastPosition, BreackForward * -1 * 100, Color.green, 100);


                        Vector3 Exitpoint = hit.point;
                        Vector3 ExitForward = coll.relativeVelocity.normalized * -1;
                        //GameObject exitpoint = Instantiate(tumor);
                        //exitpoint.transform.position = hit.point;
                        //exitpoint.transform.forward = coll.relativeVelocity.normalized * -1;
                        //print("COL");


                        //Create a triangle between the tip and base so that we can get the normal
                        Vector3 side1 = Exitpoint - Breackpoint;
                        Vector3 side2 = Exitpoint - coll.transform.position;

                        //Get the point perpendicular to the triangle above which is the normal
                        Vector3 normal = Vector3.Cross(side1, side2).normalized;

                        //Transform the normal so that it is aligned with the object we are slicing's transform.
                        Vector3 transformedNormal = ((Vector3)(this.gameObject.transform.localToWorldMatrix.transpose * normal)).normalized;

                        //Get the enter position relative to the object we're cutting's local transform
                        Vector3 transformedStartingPoint = this.gameObject.transform.InverseTransformPoint(Breackpoint);

                        Plane plane = new Plane();

                        plane.SetNormalAndPosition(
                                transformedNormal,
                                transformedStartingPoint);

                        var direction = Vector3.Dot(Vector3.up, transformedNormal);

                        //Flip the plane so that we always know which side the positive mesh is on
                        if (direction < 0)
                        {
                            plane = plane.flipped;
                        }

                        GameObject[] slices = Slicer.Slice(plane, this.gameObject);
                        Destroy(this.gameObject);

                        Rigidbody rigidbody = slices[1].GetComponent<Rigidbody>();
                        Vector3 newNormal = transformedNormal + Vector3.up * CutJumpForce;
                        rigidbody.AddForce(newNormal, ForceMode.Impulse);

                    }
                    else
                    {
                        //print("NO");
                        Debug.DrawRay(startRaycastPosition, BreackForward*-1 * 100, Color.red, 100);
                    }




                    StopAllCoroutines();
                    StartCoroutine("Intangible");
                }
            }
        }
    }

    IEnumerator Intangible()
    {
        yield return new WaitForSeconds(cutCD);
        flag = true;
        this.transform.gameObject.layer = 0;
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(deathTimer);
        Destroy(this.gameObject);
    }
}
